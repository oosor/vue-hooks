export interface OnBeforeDestroy {
    beforeDestroy(): void;
}
