export interface OnMounted {
    mounted(): void;
}
