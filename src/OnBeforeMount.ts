export interface OnBeforeMount {
    beforeMount(): void;
}
