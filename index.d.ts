export {
    OnBeforeCreate,
    OnBeforeDestroy,
    OnBeforeMount,
    OnBeforeUpdate,
    OnCreated,
    OnDestroyed,
    OnMounted,
    OnUpdated,
} from './src';